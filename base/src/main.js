import { createApp } from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';

import { registerMicroApps, start } from 'qiankun';
import _ from 'lodash';
import ElementPlus from 'element-plus';
import 'element-plus/dist/index.css';
createApp(App).use(store).use(router).use(ElementPlus).mount('#appBase');
router.beforeEach((to, from, next) => {
  if (_.isEmpty(history.state.current)) {
    _.assign(history.state, { current: from.fullPath });
  }
  next();
});
registerMicroApps(
  [
    {
      name: 'vue3',
      entry: '//localhost:8030',
      container: '#container',
      activeRule: '/vue3',
      loader: (loading) => {
        console.log(loading);
      },
    },
    {
      name: 'vue2',
      entry: '//localhost:8020',
      container: '#container',
      activeRule: '/vue2',
      loader: (loading) => {
        console.log(loading);
      },
    },
    {
      name: 'html5',
      entry: '//localhost:5501/html5/index.html',
      container: '#container',
      activeRule: '/html5',
      loader: (loading) => {
        console.log(loading);
      },
    },
  ],
  {
    beforeLoad() {
      console.log('加载前');
    },
    beforeUnmount() {
      console.log('卸载前');
    },
    beforeMount() {
      console.log('渲染前');
    },
    afterUnmount() {
      console.log('卸载后');
    },
    afterMount() {
      console.log('渲染后');
    },
  }
);
start({
  sandbox: {
    experimentalStyleIsolation: true,
    // strictStyleIsolation: true,
  },
});
