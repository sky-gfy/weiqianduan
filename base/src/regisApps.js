import { registerMicroApps, start } from 'qiankun';
const apps = [
  {
    name: 'vue3',
    entry: '//localhost:8030',
    container: '#app',
    activeRule: '/vue3',
    loader: (loading) => {
      console.log(loading);
    },
  },
  {
    name: 'purehtml',
    entry: '//localhost:5501',
    container: '#app1',
    activeRule: '/purehtml',
    loader: (loading) => {
      console.log(loading);
    },
  },
];
registerMicroApps(apps, {
  beforeLoad() {
    console.log('加载前');
  },
  beforeUnmount() {
    console.log('卸载前');
  },
  beforeMount() {
    console.log('渲染前');
  },
  afterUnmount() {
    console.log('卸载后');
  },
  afterMount() {
    console.log('渲染后');
  },
});
console.log('123123');
start();
