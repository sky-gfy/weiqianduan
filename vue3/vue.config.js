const { defineConfig } = require('@vue/cli-service');
const packageName = require('./package.json').name;
module.exports = defineConfig({
  transpileDependencies: true,
  devServer: {
    port: '8030',
    headers: {
      'Access-Control-Allow-Origin': '*',
    },
  },
  output: {
    library: `${packageName}-[name]`,
    libraryTarget: 'umd',
    chunkLoadingGlobal: `webpackJsonp_${packageName}`,
  },
  publicPath: '//localhost:8030',
});
