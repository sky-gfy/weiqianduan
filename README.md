# all

cd base

## Project setup

```
cd base
yarn install
yarn serve
```

```
cd vue2
yarn install
yarn serve
```

```
cd vue3-js
yarn install
yarn serve
```

### Lints and fixes files

```
yarn lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
