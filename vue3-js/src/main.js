import { createApp } from 'vue';
import { createWebHistory, createRouter } from 'vue-router';
import App from './App.vue';
import routes from './router';
import store from './store';
let history;
let app;
let router;
function render(props = {}) {
  const base = window.__POWERED_BY_QIANKUN__ ? '/vue3/' : '/',
    history = createWebHistory(base);
  router = createRouter({
    history,
    routes,
  });
  app = createApp(App);
  let { container } = props;
  app
    .use(router)
    .use(store)
    .mount(container ? container.querySelector('#app') : '#app');
}
if (window.__POWERED_BY_QIANKUN__) {
  __webpack_public_path__ = window.__INJECTED_PUBLIC_PATH_BY_QIANKUN__;
}

if (!window.__POWERED_BY_QIANKUN__) {
  render();
}

export async function bootstrap() {}
export async function mount(props) {
  console.log(props);
  render(props);
}
export async function unmount(props) {
  history = null;
  app.unmount();
  router = null;
}
