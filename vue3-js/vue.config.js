const { defineConfig } = require('@vue/cli-service');
const packageName = require('./package.json').name;

module.exports = defineConfig({
  transpileDependencies: true,
  devServer: {
    port: '8030',
    headers: {
      'Access-Control-Allow-Origin': '*',
    },
  },
  configureWebpack: {
    //需要获取打包的内容 umd格式
    output: {
      library: `${packageName}-[name]`,
      libraryTarget: 'umd',
      chunkLoadingGlobal: `webpackJsonp_${packageName}`,
    },
  },
  //保证子应用静态资源都是想父端口上发送的
  publicPath: '//localhost:8030',
});
